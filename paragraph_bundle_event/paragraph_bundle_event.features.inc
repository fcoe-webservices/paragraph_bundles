<?php
/**
 * @file
 * paragraph_bundle_event.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function paragraph_bundle_event_paragraphs_info() {
  $items = array(
    'event' => array(
      'name' => 'Event',
      'bundle' => 'event',
      'locked' => '1',
    ),
  );
  return $items;
}
