<?php
/**
 * @file
 * paragraph_bundle_text.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function paragraph_bundle_text_paragraphs_info() {
  $items = array(
    'text' => array(
      'name' => 'Text',
      'bundle' => 'text',
      'locked' => '1',
    ),
  );
  return $items;
}
